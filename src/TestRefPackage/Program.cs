﻿using System;
using TestBuildPackage;

namespace TestRefPackage
{
    class Program
    {
        static void Main(string[] args)
        {
            var now = new Now();
            var localTime = now.Get(TimeType.Local);
            var utcTime = now.Get(TimeType.Utc);
            Console.WriteLine("Time Now:");
            Console.WriteLine(localTime);
            Console.WriteLine(utcTime);
        }
    }
}