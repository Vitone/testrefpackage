var target = Argument("target", "Default");
var configuration = Argument("configuration", "Release");

var solution = File("./TestRefPackage.sln");
var project = File("./src/TestRefPackage/TestRefPackage.csproj");

Task("Clean")
    .Does(() =>
{
    CleanDirectories("./src/**/obj");
    CleanDirectories("./src/**/bin");
    CleanDirectories("./artifacts/");
});

Task("Restore-NuGet-Packages")
    .IsDependentOn("Clean")
    .Does(() =>
{
    NuGetRestore(solution);
});

Task("Build")
    .IsDependentOn("Restore-NuGet-Packages")
    .Does(() =>
{
    //var version = XmlPeek(project, "//PackageVersion");
    //Information("Version: {0}", version);

    Information("Restoring {0}", solution);
    NuGetRestore(solution, new NuGetRestoreSettings { NoCache = true });

    DotNetCoreBuild(solution, new DotNetCoreBuildSettings()
    {
        Configuration = configuration
    });

    DotNetCoreBuild(project, new DotNetCoreBuildSettings()
    {
        Configuration = configuration
    });
});

Task("Pack")
    .IsDependentOn("Build")
    .Does(() =>
{
    var settings = new DotNetCorePackSettings
    {
        OutputDirectory = "./artifacts/",
        NoBuild = true,
        Configuration = configuration
    };

	DotNetCorePack(project, settings);
});

Task("Default")
    .IsDependentOn("Build");

RunTarget(target);
